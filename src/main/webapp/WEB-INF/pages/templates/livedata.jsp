<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- Top -->

<div class="top" ng-style="theight">

	<!-- Header in top div -->
	<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
		<div flex="100" class="box1">
			<div id="layoutContainer" layout="row" ng-cloak layout-wrap
				layout-align="start">
				<a href="#">Home</a> > <a href="#">Device</a> >
				{{devices[currentDeviceIndex].name}}
			</div>
		</div>
		<!-- <div flex="30" class="box1">
							<div id="layoutContainer" layout="row" ng-cloak layout-wrap
								layout-align="end">
								<a href="#">Change Device</a>
							</div>
						</div> -->


		<div flex="15" class="box2">
			<div id="layoutContainer" layout="row" ng-cloak layout-wrap
				layout-align="start" style="padding-top: 8.5%;">
				<b>Device Live Data</b>
			</div>
		</div>
		<div flex="85" class="box2">
			<md-button ng-click=showhide()> <span
				class="menu-icon fa fa-bars"></span> Show and Hide Tables</md-button>
			<md-button ng-click="new_chart_dialogbox()"> <span
				class="menu-icon fa fa-line-chart"></span> Add Chart</md-button>
			<md-button ng-click="refresh()"> <span
				class="menu-icon fa fa-refresh"></span> Refresh</md-button>
			<md-button ng-click="d_settings()"> <span class="menu-icon fa fa-table"></span>
			Settings</md-button>
			<!-- <md-button> <span class="menu-icon fa fa-file-pdf-o"></span>
							PDF</md-button> -->
			<md-button ng-click="d_export()"> <span class="menu-icon fa fa-cart-plus"></span>
			Export</md-button>
			
			<!-- <md-button ng-click="useCase1()"> <span class="menu-icon glyphicon glyphicon-dashboard"></span>
			Use Case1</md-button> -->
			
			<md-button href = "#/nodejsPowerBIDatashboard"> <span class="menu-icon glyphicon glyphicon-dashboard"></span>
			SMART WAREHOUSE</md-button>
			
			<md-button href = "http://iotspherewebapp.azurewebsites.net/EY-Web/dashboard"> <span class="menu-icon glyphicon glyphicon-dashboard"></span>
			AssetTracking</md-button>
			
			<!-- <md-button href = "#/nodejsPowerBIEstimationDatashboard"> <span class="menu-icon glyphicon glyphicon-dashboard"></span>
			Use Case2</md-button> -->
			
			
			
			
		</div>
	</div>
	<!-- End Header in top div -->

	<!-- Tab Pane -->
	<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
		<div flex="100">
			<md-content> <md-tabs md-dynamic-height md-border-bottom
				class="somecol" md-selected="selectedIndex"> <!-- dynamic tabs -->
			<md-tab
				ng-repeat="tab in devices[currentDeviceIndex].tabs track by $index"
				md-on-select="onTabChanges($index)" ng-disabled="tab.disabled"
				label="{{tab.title}}">
			<div ng-include="'templates/graph'"></div>
			<%-- <br /><md-button class="md-primary md-raised"
									ng-click="removeTab( tab )" ng-disabled="devices[currentDeviceIndex].tabs.length <= 1">Remove
								Tab</md-button> --%> </md-tab> </md-tabs> <!-- Buttons  --> <!-- <section layout="row" layout-sm="column" layout-align="left center"
						layout-wrap>
						<md-button>Save</md-button>
						<md-button>Save As</md-button>
						<md-button>Compression</md-button>
						<md-button>Bandwidth</md-button>
						<md-button>Traffic</md-button>
					</section> --> <!-- End Buttons --> </md-content>
		</div>
	</div>
	<!-- End Tab Pane -->

</div>
<!-- End Top -->

<!-- Bottom -->

<div class="bottom" ng-show=sh>
	<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
		<div flex="100" class="box2">
			<md-button> <span class="menu-icon fa fa-trash"></span>
			Delete </md-button>
			<md-button> <span class="menu-icon fa fa-upload"></span>
			Update </md-button>
			<md-button> <span class="menu-icon fa fa-retweet"></span>
			Reload </md-button>
			<md-button> <span class="menu-icon fa fa-share-square-o"></span>
			Restore </md-button>
			<md-button> <span class="menu-icon fa fa-refresh"></span>
			Refresh </md-button>
		</div>
		<div flex="100" class="box1">
			<b>Device Info</b>
		</div>
	</div>
	<div class="md-padding">
		<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
			<div flex="33">
				<table class="table">
					<tr>
						<td><b>Status:</b></td>
						<td><div
								ng-style="changeColor(devices[currentDeviceIndex].device_table_data.devstatus)">{{devices[currentDeviceIndex].device_table_data.devstatus}}</div></td>
					</tr>
					<tr>
						<td><b>Alarm Status:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.alarm}}</td>
					</tr>
					<tr>
						<td><b>Product ID:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.productid}}</td>
					</tr>
					<tr>
						<td><b>Type:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.type}}</td>
					</tr>
					<tr>
						<td><b>Upload data:</b></td>
						<td>10kb</td>
					</tr>
					<tr>
						<td><b>Download Data:</b></td>
						<td>100kb</td>
					</tr>
				</table>
			</div>
			<div flex="33">
				<table class="table">
					<tr>
						<td><b>Primary IP:</b></td>
						<td>${ipaddress }</td>
					</tr>
					<tr>
						<td><b>Primary Hostname:</b></td>
						<td>${hostname }</td>
					</tr>
					<tr>
						<td><b>Physical Mac Address:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.mac}}</td>
					</tr>
					<tr>
						<td><b>Gateway:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.gateway}}</td>
					</tr>
					<tr>
						<td><b>Firmware Version:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.soft_ver}}</td>
					</tr>
					<tr>
						<td><b>Operating Band:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.operatingband}}</td>
					</tr>
				</table>
			</div>
			<div flex>
				<table class="table">
					<tr>
						<td><b>Connection Time:</b></td>
						<td><timer interval="1000">{{hours}}:{{minutes}}:{{seconds}}</timer></td>
					</tr>
					<tr>
						<td><b>Poll Frequency:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.poll}}</td>
					</tr>
					<tr>
						<td><b>Data Rate:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.datarate}}</td>
					</tr>
					<tr>
						<td><b>Health:</b></td>
						<td><div ng-style="changeColor('Green')">{{devices[currentDeviceIndex].device_table_data.health}}</div></td>
					</tr>
					<tr>
						<td><b>Power:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.power}}</td>
					</tr>
					<tr>
						<td><b>TX Power:</b></td>
						<td>{{devices[currentDeviceIndex].device_table_data.tx_power}}</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- End Bottom -->