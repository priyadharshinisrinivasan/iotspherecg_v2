<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script type="text/ng-template" id="new_chart">
	<div class="ngdialog-message">
		<h3>Add New Chart</h3>
		<br />
		<table width=100% style="line-height:30px;">
			<tr>
				<td>Tab Name:</td>
				<td><input type="text" ng-model="newdata.new_Tab_name"
					ng-init="newdata.new_Tab_name = 'New Tab'" required
					style="width: 100%;" /></td>
			</tr>
			<tr>
				<td>Number of Chart:</td>
				<td><select ng-model="newdata.selectChart"
					ng-init="newdata.selectChart='1'" required style="width: 100%;">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
				</select></td>
			</tr>
			<tr>
				<td colspan=2>
					<div ng-repeat="x in range(1,newdata.selectChart)">
						<h4>Chart {{x}}:</h4>
						<table width=100%>
							<tr>
								<td width=30%>Property:</td>
								<td width=70%><select ng-model="newdata.chartProperty[x-1]"
									ngRequired="true" required style="width: 100%;">
										<option
											ng-repeat="(key, value) in devices[currentDeviceIndex].device_graph_data"
											value="{{key}}">{{key}}</option>
								</select></td>
							</tr>
							<tr>
								<td>Color :</td>
								<td><input type="color" ng-model="newdata.color[x-1]"
									ng-init="newdata.color[x-1]='#ff0000'" required />
								<td>
							</tr>
							<tr>
								<td>Alert Option:</td>
								<td>Activate: <input type="checkbox" ng-model="newdata.alert[x-1].status" required /> 
								<labal ng-show="newdata.alert[x-1].status">External Link: <input type="checkbox" ng-model="newdata.alert[x-1].ext.status" required /> </labal>
								</td>
							<tr>
							<tr ng-show="newdata.alert[x-1].status">
								<td>Threshold Type:</td>
								<td><input type="checkbox" ng-model="newdata.alert[x-1].threshold.type.min"/> Minimum Value 
								<input type="checkbox" ng-model="newdata.alert[x-1].threshold.type.max"/> Maximum Value 
								<input type="checkbox" ng-model="newdata.alert[x-1].threshold.type.xct"/> Exact Value</td>
							<tr>
							<tr ng-show="newdata.alert[x-1].status && (newdata.alert[x-1].threshold.type.min || newdata.alert[x-1].threshold.type.max || newdata.alert[x-1].threshold.type.xct)">
								<td>Threshold Type:</td>
								<td><div id="layoutContainer" layout="row" ng-cloak layout-wrap>
								<div ng-show="newdata.alert[x-1].threshold.type.min" flex="33">Minimum: <input type="textbox" ng-model="newdata.alert[x-1].threshold.value.min" style="width:40%;"/></div>
								<div ng-show="newdata.alert[x-1].threshold.type.max" flex="33">Maximum: <input type="textbox" ng-model="newdata.alert[x-1].threshold.value.max" style="width:40%;"/></div>
								<div ng-show="newdata.alert[x-1].threshold.type.xct" flex="33">Exact: <input type="textbox" ng-model="newdata.alert[x-1].threshold.value.xct" style="width:40%;"/></div>
								</div><br />
								</td>
							<tr ng-show="newdata.alert[x-1].status && newdata.alert[x-1].ext.status">
								<td>External Link:
								</td>
								<td><input type="textbox" ng-model="newdata.alert[x-1].ext.link" ng-init='"https://t7sga0thzd.execute-api.us-west-2.amazonaws.com/PAYD"' style="width:100%;"/>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="ngdialog-buttons">
		<button type="button"
			class="ngdialog-button ngdialog-button-secondary"
			ng-click="closeThisDialog('Cancel')">Cancel</button>
		<button type="button" class="ngdialog-button ngdialog-button-primary"
			ng-click="confirm(newdata)">Add</button>
	</div>
</script>
<script type="text/ng-template" id="d_settings">
<div class="ngdialog-message">
		<h3>Settings</h3>
<b> Calibration Details</b><br/>
Device Id: <b>{{calibration.Device_Id}}</b>
<!--<br/>
<b>Old Entry</b><br/>
<table>
<tr><th>Property</th><th>Value</th></tr>
 <tr ng-repeat="(key, value) in calibration_old.property">
    <td><b>{{ key }}</b></td>
    <td>{{ value }}</td>
  </tr>
</table>-->
<br/>
<b>Calibration</b><br/>
<table>
<tr><td>Property: </td><td><select ng-model="calibration.property" ng-change="calpropchange(calibration.property)"
									ngRequired="true" required style="width: 100%;">
										<option
											ng-repeat="(key, value) in devices[currentDeviceIndex].device_graph_data"
											value="{{key}}">{{key}}</option>
								</select></td></tr>
<tr><td>Value: </td><td><input type="number" ng-model="calibration.value" ng-show="calibration.show">
<div ng-hide="calibration.show">{{calibration.showmsg}}</div></td></tr>
</div>
</table>
<div class="ngdialog-buttons">
		<button type="button"
			class="ngdialog-button ngdialog-button-secondary"
			ng-click="closeThisDialog('Cancel')">Cancel</button>
		<button type="button" class="ngdialog-button ngdialog-button-primary"
			ng-click="confirm('Done')">Done</button>
	</div>
</script>
<script type="text/ng-template" id="d_export">
<div class="ngdialog-message">
		<h3>Export</h3>
</div>
<div class="ngdialog-buttons">
		<button type="button"
			class="ngdialog-button ngdialog-button-secondary"
			ng-click="closeThisDialog('Cancel')">Cancel</button>
		<button type="button" class="ngdialog-button ngdialog-button-primary"
			ng-click="confirm('Export')">Export</button>
	</div>
</script>
<script type="text/ng-template" id="d_help">
<div class="ngdialog-message">
		<h3>Help</h3>
</div>
<div class="ngdialog-buttons">
		<button type="button" class="ngdialog-button ngdialog-button-primary"
			ng-click="confirm('ok')">OK</button>
	</div>
</script>
<script type="text/ng-template" id="d_bulkupload">
<div class="ngdialog-message">
<h3>Bulk Upload</h3>
<!-- <form action="${pageContext.request.contextPath}/bulkupload" method="POST" enctype="multipart/form-data" id = "upform">-->
Select option: <br/>Device: <input type="radio" ng-model="choice.c" value="dev"><br/> 
Gateway: <input type="radio" ng-model="choice.c" value="gateway"><br/>
Select File: <input type="file" id="file" name="file"><br/>
<!-- <input type="submit" value="Upload Devices" name="submit"> -->
</form>
</div>
<div class="ngdialog-buttons">
		<button type="button"
			class="ngdialog-button ngdialog-button-secondary"
			ng-click="closeThisDialog('Cancel')">Cancel</button>
		<button type="button" class="ngdialog-button ngdialog-button-primary"
			ng-click="confirm('Done')" ng-disabled="bulkupbut">Done</button>
	</div>
</script>
<!-- <script type="text/ng-template" id="d_deviceregister">
<div class="ngdialog-message">
		<h3>Device Register</h3>
<form action="${pageContext.request.contextPath}/registerdevice" method="POST" enctype="multipart/form-data">
<input type="text" name="Device_Id">
<input type="text" name="Gateway_Id">
<input type="submit" value="Add Devices" name="submit">
</form>
</div>
<div class="ngdialog-buttons">
		<button type="button"
			class="ngdialog-button ngdialog-button-secondary"
			ng-click="closeThisDialog('Cancel')">Cancel</button>
		<button type="button" class="ngdialog-button ngdialog-button-primary"
			ng-click="confirm('Done')">Done</button>
	</div>
</script>-->
<script type="text/ng-template" id="d_manage">
<div class="ngdialog-message">
		<h3>Device Management</h3>
<li><a ng-click="d_bulkupload();confirm('Done')">Bulk Upload</a></li>
<li><a ng-click="confirm('Done')" href = "#/newdevice">Add New Device</a></li>
<li><a ng-click="confirm('Done')" href="#/newgateway">Add New Gateway</a></li>
</div>
<div class="ngdialog-buttons">
		<button type="button"
			class="ngdialog-button ngdialog-button-secondary"
			ng-click="closeThisDialog('Cancel')">Cancel</button>
		<button type="button" class="ngdialog-button ngdialog-button-primary"
			ng-click="confirm('Done')">Done</button>
	</div>
</script>
<script type="text/ng-template" id="devicemanagement">
<div class="ngdialog-message">
<h1>Device Management</h1>
<table><tr><td>
Device ID:</td><td><input type='textbox' id="dev_id" name="Device_Id"/></td></tr>
<tr><td>
Gateway ID:</td><td>
<select name="Gateway_Id" id="gateway_id" ng-model='gw' ng-init="gw = gateway.gateways[0]" ng-options="o as o for o in gateway.gateways"></select>
</td></tr><td></td><td>
</table>
</div>
<div class="ngdialog-buttons">
		<button type="button"
			class="ngdialog-button ngdialog-button-secondary"
			ng-click="closeThisDialog('Cancel')">Cancel</button>
		<button type="button" class="ngdialog-button ngdialog-button-primary"
			ng-click="confirm('Done')">Done</button>
	</div>
</script>
<script type="text/ng-template" id="registergateway">
<h1> Gateway Registration </h1>
Gateway ID: <input type='textbox' id="id" name="Gateway_ID"/><br/>
Gateway Product ID: <input type='textbox' id="pid" name="Gateway_pid"/><br/>
Gateway Type: <input type='textbox' id="type" name="type"/><br/>
Model Number: <input type='textbox' id="model" name="firmware_ver"/><br/>
<div class="ngdialog-buttons">
		<button type="button"
			class="ngdialog-button ngdialog-button-secondary"
			ng-click="closeThisDialog('Cancel')">Cancel</button>
		<button type="button" class="ngdialog-button ngdialog-button-primary"
			ng-click="confirm('Done')">Done</button>
	</div>
</script>

<!-- <script type="text/ng-template" id="d_search">
<h1> Gateway Registration </h1>
Search: <input type='textbox' id="searchKey" ng-model="searchKey.text" name="searchKey" required />
<button ng-click=search()>Search</button>
<div class="ngdialog-buttons">
		<button type="button"
			class="ngdialog-button ngdialog-button-secondary"
			ng-click="closeThisDialog('Cancel')">Cancel</button>
		<button type="button" class="ngdialog-button ngdialog-button-primary"
			ng-click="confirm('Done')">Done</button>
	</div>
</script>
t.d_searchfunction = function() {
		
		nd.openConfirm({
			template : 'd_search',
			className : 'ngdialog-theme-default',
			width : '40%',
			scope : t,
		}).then(function(value) {
			console.log('resolved:' + value);
			// Perform the save here
		}, function(value) {

			console.log('rejected:' + value);

		});

	};
	 -->
	 
	 <script type="text/ng-template" id="displayMoreDevices">
<div class="ngdialog-message table-responsive" style="background-color:black;color:white;height:300px;">


                              <table>
                                    
                                    <thead style="background-color:#110000;">
                                          <tr>
                                                <th>Sr.Id</th>
                                                <th>Device Id</th>
                                                <th>Gateway</th>
                                                <th>Status</th>
                                                
                                                
                                          </tr>
                                    </thead>
                                    <tbody ng-repeat="device in devices">
                                    
                                          <tr ng-if="$index>4">

                                                <td>{{$index}}</td>
                                                <td>
                                                        <uib-accordion close-others="true">
                                                            <div uib-accordion-group class="device-menu panel-default "
                                                                  is-open="status.isFirstOpen" is-disabled="status.isFirstDisabled">
                                                                  <uib-accordion-heading>
                                                                     <b
                                                                          ng-style="{'color': device.color, 'padding-left': device.pad}">{{device.name}}</b>
                                                                     <span class="pull-right menu-icon fa fa-usb"
                                                                          ng-style="{'color': device.color, 'padding-right': device.padr}"></span><!-- <i
                                                                          class="pull-right glyphicon"
                                                                          ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"
                                                                          style="color: #E6E6DF; right: 18px;"></i> --> 
                                                                  </uib-accordion-heading>
                                                                  <div id="layoutContainer" layout="row" ng-cloak layout-wrap
                                                                     ng-repeat="item in device.items">
                                                                     <div flex="90">
                                                                          <a href="{{item.url}}"
                                                                              ng-click="devchange($parent.$index,item.url)">{{item.name}}</a>
                                                                     </div>
                                                                     <div flex>
                                                                          <a href="{{item.url}}"
                                                                              ng-click="devchange($parent.$index,item.url)"><span
                                                                              class="menu-icon fa fa-{{item.icon}}"></span></a>
                                                                     </div>
                                                                  </div>
                                                            </div>
                                                        </uib-accordion>
                                                   
                                                </td>
                                                
                                                <td>{{device.gateway}}</td>
                                                <td>{{device.status}}</td>
                                                
                                                
                                          </tr>

                                    </tbody>
                                    
                                    </div>
                              </table>
                        
</div>
</script>
	 