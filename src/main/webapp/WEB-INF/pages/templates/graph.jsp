<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<md-content class="md-padding somecol"
	style="padding-top:0; padding-bottom:0;">
<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
	<div
		ng-repeat="x in range(1,devices[currentDeviceIndex].tabs[currentTab].no_of_chart)"
		flex={{devices[currentDeviceIndex].tabs[currentTab].graph_flex_size}}>
		<nvd3
			options="devices[currentDeviceIndex].tabs[currentTab].graph[x-1]"
			data="devices[currentDeviceIndex].tabs[currentTab].graph_data[x-1].y"></nvd3>
	</div>
</div>
</md-content>