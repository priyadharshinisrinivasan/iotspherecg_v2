package com.ey.iot.iotspherecg.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.ey.iot.iotspherecg.encrypt.BCryptutil;
import com.ey.iot.iotspherecg.model.LoginModel;
import com.ey.iot.iotspherecg.service.EYService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider, AuthenticationEntryPoint {

	@Autowired
	EYService eyservice;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String name = authentication.getName();
		String password;
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		if (name.equalsIgnoreCase("sree@ey.com")) {
			password = "iot";
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		} else {
			password = authentication.getCredentials().toString();
		}
		boolean flg = false;
		List<LoginModel> LoginModel = this.eyservice.finddata(name);
		if (LoginModel.isEmpty() == false) {
			String computedhash = LoginModel.get(0).getPwd();
			flg = BCryptutil.verifypwd(password, computedhash);
		}

		if (flg == true) {
			return new UsernamePasswordAuthenticationToken(name, password, authorities);
		} else {
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		System.out.println("Executing commence method due to failed Authentication");
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized Access!!");

	}
}