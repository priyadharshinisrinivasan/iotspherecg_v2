package com.ey.iot.iotspherecg.dao;

import java.util.List;
import com.ey.iot.iotspherecg.model.AssetInputModel;

public abstract interface AssetInputDao
{  
  public abstract List<AssetInputModel> finddata(String paramString);
  
  public abstract void deletedata(String paramString);
public abstract void saveOrUpdate(AssetInputModel assetinputmodel);

}
