package com.ey.iot.iotspherecg.dao;

import java.util.List;
import com.ey.iot.iotspherecg.model.AssetOutputModel;

public abstract interface AssetOutputDao
{  
  public abstract List<AssetOutputModel> finddata(String paramString);
  
  public abstract void deletedata(String paramString);
public abstract void saveOrUpdate(AssetOutputModel assetoutputmodel);

}
