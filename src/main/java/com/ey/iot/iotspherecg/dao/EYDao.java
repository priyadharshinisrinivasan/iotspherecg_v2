package com.ey.iot.iotspherecg.dao;

import java.util.List;

import com.ey.iot.iotspherecg.model.LoginModel;

public abstract interface EYDao
{  
  public abstract List<LoginModel> finddata(String paramString);

public abstract void saveOrUpdate(LoginModel loginmodel);

}
