package com.ey.iot.iotspherecg.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.ey.iot.iotspherecg.model.AssetInputModel;

@Transactional
@Repository
public class AssetInputDaoImpl implements AssetInputDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<AssetInputModel> finddata(String input) {
		Criteria criteria = ((Session)this.entityManager.unwrap(Session.class)).createCriteria(AssetInputModel.class);
	//	criteria.addOrder(Order.desc("r_cre_time"));
		if (input != "") {
			criteria.add(Restrictions.eq("orderid", input));
		}
		return criteria.list();
	}



	public void deletedata(String ssn) {
		//String databasetype = this.env.getProperty("jdbc.databasetype");
		//System.out.println(databasetype);
		Query query;
		//if (databasetype.equalsIgnoreCase("MYSQL")) {
			query = ((Session)this.entityManager.unwrap(Session.class)).createSQLQuery("DELETE FROM ASSETINPUTINFO");
		//} else {
		//	query = ((Session)this.entityManager.unwrap(Session.class)).createSQLQuery(
		//			"DELETE FROM JYMAIN WHERE R_CRE_TIME in (SELECT TOP 3 R_CRE_TIME FROM JYMAIN ORDER BY R_CRE_TIME ASC)");
		//}
		query.executeUpdate();
	}



	@Override
	@Transactional
	public void saveOrUpdate(AssetInputModel assetinputmodel) {
		Session sess = ((Session)this.entityManager.unwrap(Session.class));
		//sess.saveOrUpdate(assetinputmodel);
		//	entityManager.persist(assetinputmodel);
			sess.beginTransaction();
			sess.save(assetinputmodel);
		}
		
	
}
