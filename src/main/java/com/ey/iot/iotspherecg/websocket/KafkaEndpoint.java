package com.ey.iot.iotspherecg.websocket;


import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.stereotype.Component;
import com.ey.iot.iotspherecg.configuration.AppProperty;
import org.apache.kafka.clients.consumer.*;
import java.util.Arrays;
import java.util.Properties;

@Component
public class KafkaEndpoint {


	public static String KAFKA_ENABLED_VAL=(new AppProperty()).getPropertyValue("prop.KAFKA_ENABLED");
	public static String KAFKA_TOPIC_VAL=(new AppProperty()).getPropertyValue("prop.KAFKA_TOPIC");
	public static String KAFKA_BOOTSTRAP_SERVERS_VAL=(new AppProperty()).getPropertyValue("prop.KAFKA_BOOTSTRAP_SERVERS");

	
	public StompSession stompSession = null;
	public String websocket_uri;
	public String microservice_auth;
	public String token = null;
	public static StompClientLocal ws_local;
	/*public StompClientDotNet ws_dotnet;
	public StompClientNodeJS ws_nodejs;*/
	public boolean setup = false;
	public String kakfa_enabled="N";

	public KafkaEndpoint() {

	
	}

	public void setup(StompClientLocal ws_local) throws InterruptedException {
		this.ws_local = ws_local;
		runConsumer();
		
}

	static void runConsumer()
	{
		  String topic = KAFKA_TOPIC_VAL;
	      String group = "test-consumer-group";
	      Properties props = new Properties();
	      props.put("bootstrap.servers", KAFKA_BOOTSTRAP_SERVERS_VAL);
	      props.put("group.id", group);
	      props.put("enable.auto.commit", "true");
	      props.put("auto.commit.interval.ms", "1000");
	      props.put("session.timeout.ms", "30000");
	      props.put("key.deserializer",          
	         "org.apache.kafka.common.serialization.StringDeserializer");
	      props.put("value.deserializer", 
	         "org.apache.kafka.common.serialization.StringDeserializer");
		 KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
		try
		{
	      consumer.subscribe(Arrays.asList(topic));
	      System.out.println("Subscribed to kafka topic " + topic);
	      while (true) {
	         ConsumerRecords<String, String> records = consumer.poll(2000);
	            for (ConsumerRecord<String, String> record : records)
	            {
	               System.out.printf("offset = %d, key = %s, value = %s\n", 
	               record.offset(), record.key(), record.value());
	               String stompjson = "{ \"sender\" : \"WebappIoTHub\",\"type\" : \"CHAT\",\"content\" : " + record.value() + "}";
                   ws_local.sendDeviceData(ws_local.stompSession, stompjson);    
	            }
	      }  
		}
		catch(Exception e)
		{
			consumer.close();
		}
	}
	
	
}
