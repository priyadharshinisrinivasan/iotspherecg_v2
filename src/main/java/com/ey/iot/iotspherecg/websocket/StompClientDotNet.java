package com.ey.iot.iotspherecg.websocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import com.ey.iot.iotspherecg.configuration.AppProperty;
import com.ey.iot.iotspherecg.model.DeviceMessage;

@Component
public class StompClientDotNet {

	private Logger logger = Logger.getLogger(this.getClass());

	public StompSession stompSession = null;
	public String websocket_dotnet_uri;
	public String microservice_dotnet_auth;
	public String token = null;
	public boolean setup = false;

	public StompClientDotNet() {
		this.websocket_dotnet_uri = (new AppProperty()).getPropertyValue("websocket_dotnet_uri");
		this.microservice_dotnet_auth = (new AppProperty()).getPropertyValue("microservice_dotnet_auth");
		/*
		 * this.token =
		 * "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtaWMuc2VydkBleS5jb20iLCJyb2xlcyI6InVzZX" +
		 * "IiLCJpYXQiOjE1MTY2MjY2Njd9.aLufagzdFM6QOhhvmahHM1aiL4HvIstyetJ9YPNyftI";
		 */
	}

	private void connect() {
		WebSocketClient client = new StandardWebSocketClient();
		WebSocketStompClient stompClient = new WebSocketStompClient(client);
		stompClient.setMessageConverter(new MappingJackson2MessageConverter());
		StompSessionHandler sessionHandler = new MyStompSessionHandler();
		StompHeaders headers = new StompHeaders();
		headers.add("Authorization", "Bearer " + this.token);
		ListenableFuture<StompSession> f = stompClient.connect(this.websocket_dotnet_uri, new WebSocketHttpHeaders(),
				headers, sessionHandler);
		try {
			this.stompSession = f.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	public void sendDeviceData(StompSession stompSession, Object string) {
		// String jsonHello = "{ \"sender\" : \"MicroService\",\"type\" :
		// \"CHAT\",\"content\" : " + string + "}";
		stompSession.send("/app/chat.sendMessage", string);
	}

	public void setup() {
		boolean retry;
		do {
			if (this.token == null) {
				authToken();
				retry = true;
			} else {
				if (!this.setup) {
					this.connect();
					retry = false;
				} else {
					if (!this.stompSession.isConnected())
						retry = true;
					else
						retry = false;
				}
			}
		} while (retry);
	}

	private void authToken() {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(this.microservice_dotnet_auth);
		try {
			// String userCredentials = "mic.serv@ey.com:iot";
			// String basicAuth = "Basic " + new
			// String(Base64.encode(userCredentials.getBytes()));
			String basicAuth = "Basic c3JlZUBleS5jb206aW90";
			StringEntity params = new StringEntity("dummy", "UTF-8");
			httpPost.setEntity(params);
			httpPost.setHeader("Authorization", basicAuth);
			CloseableHttpResponse response = client.execute(httpPost);
			if (response.getStatusLine().getStatusCode() == 200) {
				BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String readLine;
				String responseBody = "";
				while (((readLine = br.readLine()) != null)) {
					responseBody += readLine;
				}
				br.close();
				if (!(responseBody.equalsIgnoreCase("HTTP Status 401-Forbidden"))) {
					this.token = responseBody;
					logger.info("New Token: " + this.token);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public class MyStompSessionHandler extends StompSessionHandlerAdapter {

		private Logger logger = Logger.getLogger(this.getClass());

		@Override
		public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
			logger.info("New session established : " + session.getSessionId());
			session.subscribe("/channel/public", this);
			logger.info("Subscribed to /channel/public");
			session.send("/app/chat.sendMessage", getGreetingMessage());
			logger.info("Message sent to websocket server");
		}

		@Override
		public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload,
				Throwable exception) {
			logger.error("Got an exception", exception);
		}

		@Override
		public Type getPayloadType(StompHeaders headers) {
			return DeviceMessage.class;
		}

		@Override
		public void handleFrame(StompHeaders headers, Object payload) {
			// DeviceMessage msg = (DeviceMessage) payload;
			// logger.info("Received Local : " + msg.getContent().toJSONString());
		}

		private DeviceMessage getGreetingMessage() {
			DeviceMessage msg = new DeviceMessage();
			msg.setSender("Stomp Client Local");
			msg.setType(DeviceMessage.MessageType.JOIN);
			msg.setContent(new JSONObject());
			return msg;
		}
	}
}
