package com.ey.iot.iotspherecg.service;

import java.util.List;

import com.ey.iot.iotspherecg.model.AssetInputModel;

public abstract interface AssetInputService
{
  public abstract void savedata(AssetInputModel assetinputModel);
  
  public abstract void deletedata(String paramString);
  
  public abstract List<AssetInputModel> finddata(String paramString);
  
}
