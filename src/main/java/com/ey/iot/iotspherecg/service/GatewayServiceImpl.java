package com.ey.iot.iotspherecg.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.iot.iotspherecg.dao.GatewayDao;
import com.ey.iot.iotspherecg.model.GatewayModel;

@Service
public class GatewayServiceImpl implements GatewayService {
	@Autowired
	private GatewayDao dao;

	@Override
	public List<GatewayModel> getAllGateway() {
		return dao.getAllGateway();
	}

	@Override
	public List<GatewayModel> getGatewayById(String GatewayId) {
		List<GatewayModel> obj = dao.getGatewayById(GatewayId);
		return obj;
	}

	@Override
	public synchronized boolean addGateway(GatewayModel Gateway) {
		if (dao.gatewayExists(Gateway.getGateway_Id())) {
			return false;
		} else {
			dao.addGateway(Gateway);
			return true;
		}
	}

	@Override
	public void updateGateway(GatewayModel gateway) {
		dao.updategateway(gateway);

	}

	@Override
	public void deleteGateway(String gateway) {
		dao.deletegateway(gateway);

	}

	@Override
	public List<GatewayModel> searchGateway(String keyword) {
		return dao.searchGateway(keyword);
	}

	@Override
	public GatewayModel getGateway(String gatewayID) {
		return dao.getGateway(gatewayID);
	}

	@Override
	public boolean isGatewayExist(String string) {
		List<GatewayModel> list = dao.getGatewayById(string);
		if (list.isEmpty())
			return false;
		else
			return true;
	}

	@Override
	public void saveOrUpdate(GatewayModel gateway) {
		this.dao.saveOrUpdate(gateway);		
	}
}
