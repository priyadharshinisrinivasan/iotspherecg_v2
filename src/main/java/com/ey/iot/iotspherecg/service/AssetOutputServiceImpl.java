package com.ey.iot.iotspherecg.service;


import java.util.List;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ey.iot.iotspherecg.dao.AssetOutputDao;
import com.ey.iot.iotspherecg.model.AssetOutputModel;

@Service("AssetOutputService")
@DynamicUpdate(true)
@Transactional
public class AssetOutputServiceImpl
  implements AssetOutputService
{
  @Autowired
  private AssetOutputDao dao;
  
  public void savedata(AssetOutputModel assetoutputmodel)
  {
    this.dao.saveOrUpdate(assetoutputmodel);
  }
  
  public List<AssetOutputModel> finddata(String input)
  {
    return this.dao.finddata(input);
  }

@Override
public void deletedata(String paramString) {
	// TODO Auto-generated method stub
	
}


  
 
 
}
