package com.ey.iot.iotspherecg.service;


import java.util.List;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ey.iot.iotspherecg.dao.AssetInputDao;
import com.ey.iot.iotspherecg.model.AssetInputModel;

@Service("AssetInputService")
@DynamicUpdate(true)
@Transactional
public class AssetInputServiceImpl
  implements AssetInputService
{
  @Autowired
  private AssetInputDao dao;
  
  public void savedata(AssetInputModel assetinputmodel)
  {
    this.dao.saveOrUpdate(assetinputmodel);
  }
  
  public List<AssetInputModel> finddata(String input)
  {
    return this.dao.finddata(input);
  }

@Override
public void deletedata(String paramString) {
	// TODO Auto-generated method stub
	
}


  
 
 
}
