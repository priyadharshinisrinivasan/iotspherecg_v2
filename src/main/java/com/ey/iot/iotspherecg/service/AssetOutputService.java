package com.ey.iot.iotspherecg.service;

import java.util.List;

import com.ey.iot.iotspherecg.model.AssetOutputModel;

public abstract interface AssetOutputService
{
  public abstract void savedata(AssetOutputModel assetinputModel);
  
  public abstract void deletedata(String paramString);
  
  public abstract List<AssetOutputModel> finddata(String paramString);
  
}
