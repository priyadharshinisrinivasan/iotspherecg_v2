package com.ey.iot.iotspherecg.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Principal;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/templates")
public class UIController {
	
	@RequestMapping(value = { "/livedata" }, method = { RequestMethod.GET })
	public String dashboard_livedata(ModelMap model, Principal principal) {
		InetAddress myIP = null;
		try {
			myIP = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		String IPAddress = myIP.getHostAddress();
		String hostname = myIP.getHostName();
		model.addAttribute("hostname", hostname);
		model.addAttribute("ipaddress", IPAddress);
		return "templates/livedata";
	}

	@RequestMapping(value = { "/dashboard" }, method = { RequestMethod.GET })
	public String dashboard_dashboard(ModelMap model, Principal principal) {
		InetAddress myIP = null;
		try {
			myIP = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		String IPAddress = myIP.getHostAddress();
		String hostname = myIP.getHostName();
		model.addAttribute("hostname", hostname);
		model.addAttribute("ipaddress", IPAddress);
		return "templates/dashboard";
	}

	@RequestMapping(value = { "/about" }, method = { RequestMethod.GET })
	public String dashboard_about(ModelMap model, Principal principal) {
		return "templates/about";
	}

	@RequestMapping(value = { "/support" }, method = { RequestMethod.GET })
	public String dashboard_support(ModelMap model, Principal principal) {
		return "templates/support";
	}

	@RequestMapping(value = { "/newdevice" }, method = { RequestMethod.GET })
	public String dashboard_newdevice(ModelMap model, Principal principal) {
		return "templates/NewDevice";
	}

	@RequestMapping(value = { "/newgateway" }, method = { RequestMethod.GET })
	public String dashboard_newgateway(ModelMap model, Principal principal) {
		return "templates/NewGateway";
	}

	@RequestMapping(value = { "/index" }, method = { RequestMethod.GET })
	public String dashboard_index(ModelMap model, Principal principal) {
		return "templates/index";
	}

	@RequestMapping(value = { "/search" }, method = { RequestMethod.GET })
	public String dashboar_search(ModelMap model, Principal principal) {
		return "templates/search";
	}

	@RequestMapping(value = { "/gateway/{id}" }, method = { RequestMethod.GET })
	public String gateway(ModelMap model, Principal principal, @PathVariable("id") String gateway) {
		return "templates/gateway";
	}

	@RequestMapping(value = { "/graph" }, method = { RequestMethod.GET })
	public String dashboard_graph(ModelMap model, Principal principal) {
		return "templates/graph";
	}

	@RequestMapping(value = { "/nodejsPowerBIDatashboard" }, method = { RequestMethod.GET })
	public String nodejsPowerBIDatashboard(ModelMap model, Principal principal) {
		return "templates/nodejsPowerBIDatashboard";
	}
	
	
	@RequestMapping(value = { "/nodejsPowerBIEstimationDatashboard" }, method = { RequestMethod.GET })
	public String nodejsPowerBIEstimationDatashboard(ModelMap model, Principal principal) {
		
	   	return "templates/nodejsPowerBIEstimationDatashboard";
	}	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/refreshServer" }, method = { RequestMethod.POST })
	public @ResponseBody String refreshServer() {
		JSONObject status = new JSONObject();

		status.put("Status", "Refreshed the server");

		
		return status.toJSONString();
	}
}
