package com.ey.iot.iotspherecg.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.ey.iot.iotspherecg.configuration.AppProperty;
import com.ey.iot.iotspherecg.model.AssetInputModel;
import com.ey.iot.iotspherecg.model.AssetOutputModel;
import com.ey.iot.iotspherecg.model.Calibration_Config;
import com.ey.iot.iotspherecg.model.DeviceModel;
import com.ey.iot.iotspherecg.model.GatewayModel;
import com.ey.iot.iotspherecg.service.AssetInputService;
import com.ey.iot.iotspherecg.service.AssetOutputService;
import com.ey.iot.iotspherecg.service.Calibration_ConfigService;
import com.ey.iot.iotspherecg.service.DeviceService;
import com.ey.iot.iotspherecg.service.EYService;
import com.ey.iot.iotspherecg.service.GatewayService;
import com.ey.iot.iotspherecg.service.UserRegister;
import com.ey.iot.iotspherecg.service.UserReset;
import com.ey.iot.iotspherecg.websocket.KafkaEndpoint;
import com.ey.iot.iotspherecg.websocket.StompClientLocal;
import com.ey.iot.iotspherecg.websocket.StompClientMS;
import com.ey.iot.iotspherecg.websocket.MessageRead_IotHub;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.microsoft.azure.eventhubs.EventHubException;
import com.microsoft.azure.sdk.iot.service.Device;
import com.microsoft.azure.sdk.iot.service.exceptions.IotHubException;

@Controller
public class AppController {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	MessageRead_IotHub iothubread_client;
	
	@Autowired
	StompClientMS ws_ms;
	@Autowired
	KafkaEndpoint kafka_endpoint;
	@Autowired
	StompClientLocal ws_local;
	/*@Autowired
	StompClientDotNet ws_dotnet;
	@Autowired
	StompClientNodeJS ws_nodejs;*/
	@Autowired
	GatewayService gatewayService;
	@Autowired
	DeviceService deviceService;
	@Autowired
	EYService eyservice;
	@Autowired
	Calibration_ConfigService calibration_ConfigService;
	@Autowired
	AssetInputService assetinputservice;
	@Autowired
	AssetOutputService assetoutputservice;
	@Autowired
	@Value("${microservice}")
	private String microservice;
	private String microserviceToken;
	
	@Autowired
	@Value("${server.session.timeout}")
	private String appMaxIdleTime;
	
	@Autowired
	@Value("${prop.KAFKA_ENABLED}")
	private String kafka_enabled;
	
	@Autowired
	@Value("${nodejs.powerbi.dashboard.url}")
	private String nodeJsPowerBiDashboardUrl;
	
	
	@Autowired
	@Value("${cloud.user.email.id}")
	private String cloudUserEmailId;
	
	@Autowired
	@Value("${cloud.user.password}")
	private String cloudUserPassword;
	
	private int SESSION_TIME_MULTIPLIER = 1000;

	public void authMicroService() {

		// This method update the token for JWT from microservices

		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(microservice + "/authenticate");
		try {
			//String userCredentials = "mic.serv@ey.com:iot";
			String userCredentials = cloudUserEmailId.trim()+":"+cloudUserPassword.trim();
			String basicAuth = "Basic " + new String(Base64.encode(userCredentials.getBytes()));
			StringEntity params = new StringEntity("dummy", "UTF-8");
			httpPost.setEntity(params);
			httpPost.setHeader("Authorization", basicAuth);
			CloseableHttpResponse response = client.execute(httpPost);
			if (response.getStatusLine().getStatusCode() == 200) {
				BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String readLine;
				String responseBody = "";
				while (((readLine = br.readLine()) != null)) {
					responseBody += readLine;
				}
				br.close();
				setMicroserviceToken(responseBody);
			}
			else
			{
				logger.info("Invalid response..Dummy seed user/pwd may not have been set in DB!");
			}
		} catch (IOException e) {
			logger.info("Dummy seed user/pwd may not have been set in DB!"+e);
			e.printStackTrace();
		}
	}

	@RequestMapping("/admin")
	public String admin(Map<String, Object> map, final ModelMap model, Principal principal) {

		// Administrator Dashboard

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		boolean admin_enable = false;
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			String name = auth.getName(); // get logged in username
			Collection<? extends GrantedAuthority> aList = auth.getAuthorities();
			admin_enable = aList.contains(new SimpleGrantedAuthority("ROLE_ADMIN"));

			if (admin_enable) {
				model.addAttribute("username", name);
				return "admin";
			} else {
				return "index";
			}
		} else
			return "index";

	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody String postregister(final ModelMap model, HttpServletRequest request,
			@RequestParam("email") String emailuser, @RequestParam("password") String password,
			@RequestParam("fullname") String fullname, @RequestParam("city") String city,
			@RequestParam("country") String country, @RequestParam("address") String address,
			UriComponentsBuilder ucBuilder) throws IOException {

		// Register new User

		String msg = UserRegister.register(eyservice, model, request, emailuser, password, fullname, city, country,
				address);
		return msg;
	}

	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public @ResponseBody String reset(ModelMap model, HttpServletRequest request,
			@RequestParam("email") String emailuser, UriComponentsBuilder ucBuilder) throws IOException {

		// Reset Password

		String flg = "fail";
		String msg;
		flg = UserReset.reset(emailuser, eyservice);
		if (flg.equalsIgnoreCase("success")) {
			model.addAttribute("error", "true");
			model.addAttribute("msg2", "Password reset succesfuly.Please check your mail.");
			msg = "Password reset succesfuly.Please check your mail.";
		} else {
			model.addAttribute("error", "true");
			model.addAttribute("msg", "Error resetting password");
			model.addAttribute("msg2", "Error resetting password");
			msg = "Error resetting password";
		}
		return msg;
	}

	@RequestMapping(value = { "/loginError" }, method = { RequestMethod.GET })
	public String loginError(ModelMap model) {
		model.addAttribute("error", "true");
		model.addAttribute("msg", "Invalid login credentials");
		return "login";
	}

	@RequestMapping(value = { "/login" }, method = { RequestMethod.GET })
	public String login(ModelMap model, Principal principal) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			return "redirect:dashboard";
		}
		String name = auth.getName();
		model.addAttribute("username", name);
		return "login";
	}

	@RequestMapping(value = { "/logout" }, method = { RequestMethod.GET })
	public String logout(HttpServletRequest request) {
		HttpSession httpSession = request.getSession();
		httpSession.invalidate();
		return "login";
	}

	@RequestMapping(value = { "/home", "/" }, method = { RequestMethod.GET })
	public String defaulthome(ModelMap model, Principal principal) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			String name = auth.getName();
			model.addAttribute("username", name);
			
			/*
			 * appMaxIdleTime this into milli seconds.
			 * 
			 * Ex: 5 min = 5 * 60000 = 30000
			 */
			int appMaxIdleTimeValue = Integer.valueOf(appMaxIdleTime);
			model.addAttribute("appMaxIdleTime", appMaxIdleTimeValue * SESSION_TIME_MULTIPLIER);
			
			model.addAttribute("nodeJsPowerBiDashboardUrl", nodeJsPowerBiDashboardUrl);
			
		}
		return "home";
	}

	@RequestMapping(value = { "/dashboard" }, method = { RequestMethod.GET })
	public String dashboard(ModelMap model, Principal principal) {

		// Open Dashboard

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			String name = auth.getName();
			model.addAttribute("username", name);
		}
		String time = "" + new Date().getTime();
		model.addAttribute("timestamp", time);
		
		int appMaxIdleTimeValue = Integer.valueOf(appMaxIdleTime);
		model.addAttribute("appMaxIdleTime", appMaxIdleTimeValue * SESSION_TIME_MULTIPLIER);
		
		model.addAttribute("nodeJsPowerBiDashboardUrl", nodeJsPowerBiDashboardUrl);
		
		return "dashboardindex";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/getgateways" }, method = { RequestMethod.POST })
	public @ResponseBody String getgw() {

		// Return all the Gateways

		List<GatewayModel> gw = gatewayService.getAllGateway();
		List<String> gwstr = new ArrayList<String>();
		JSONObject gwjson = new JSONObject();
		for (GatewayModel g : gw) {
			gwstr.add(g.getGateway_Id());
		}
		gwjson.put("gateways", gwstr);
		return gwjson.toJSONString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getdevices", method = RequestMethod.POST)
	public @ResponseBody String getdev(@RequestParam(value = "gateway") String gateway) {

		// Return all connected devices to a gateway

		List<DeviceModel> dev = deviceService.getDeviceByGatewayId(gateway);
		JSONObject resp = new JSONObject();
		try {
			resp.put("devices", new JSONParser().parse(new Gson().toJson(dev)));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return resp.toJSONString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/devicebulkupload" }, method = { RequestMethod.POST })
	public @ResponseBody String deviceBulkUpload(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "file") MultipartFile file) {

		// Upload devices in bulk

		int noOfTry = 0;
		String status = "";
		do {
			CloseableHttpClient client = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(microservice + "/devicebulkupload");
			try {
				MultipartEntityBuilder builder = MultipartEntityBuilder.create();
				builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
				builder.addBinaryBody("file", file.getBytes(), ContentType.parse(file.getContentType()),
						file.getOriginalFilename());
				HttpEntity entity = builder.build();
				httpPost.setEntity(entity);
				if (getMicroserviceToken() == null)
					authMicroService();
				httpPost.setHeader("Authorization", "Bearer " + getMicroserviceToken());
				CloseableHttpResponse response = client.execute(httpPost);
				if (response.getStatusLine().getStatusCode() == 200) {
					BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					String readLine;
					String responseBody = "";
					while (((readLine = br.readLine()) != null)) {
						responseBody += readLine;
					}
					br.close();

					if ((response.toString()).equalsIgnoreCase("Invalid Token")) {
						JSONObject js = new JSONObject();
						js.put("Status", "Auth Error: Invalid Token");
						status = js.toJSONString();
						authMicroService();
						noOfTry += 1;
					} else {
						JSONObject js = new JSONObject();
						js.put("Status", responseBody);
						status = js.toString();
						break;
					}
				}
			} catch (IOException e) {
				JSONObject js = new JSONObject();
				js.put("Status_error", "Error while uploading to Cloud");
				status = js.toString();
				e.printStackTrace();
			}
			logger.info(status);
		} while (noOfTry < 3);
		return status;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/gatewaybulkupload" }, method = { RequestMethod.POST })
	public @ResponseBody String gatewayBulkUpload(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "file") MultipartFile file) {

		// Uploade Gateways in bulk

		String status = "";
		int noOfTry = 0;
		do {
			CloseableHttpClient client = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(microservice + "/gatewaybulkupload");
			try {
				MultipartEntityBuilder builder = MultipartEntityBuilder.create();
				builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
				builder.addBinaryBody("file", file.getBytes(), ContentType.parse(file.getContentType()),
						file.getOriginalFilename());
				HttpEntity entity = builder.build();
				httpPost.setEntity(entity);
				if (getMicroserviceToken() == null)
					authMicroService();
				httpPost.setHeader("Authorization", "Bearer " + getMicroserviceToken());
				CloseableHttpResponse response = client.execute(httpPost);
				if (response.getStatusLine().getStatusCode() == 200) {
					BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					String readLine;
					String responseBody = "";
					while (((readLine = br.readLine()) != null)) {
						responseBody += readLine;
					}
					br.close();
					if ((response.toString()).equalsIgnoreCase("Invalid Token")) {
						JSONObject js = new JSONObject();
						js.put("Status", "Auth Error: Invalid Token");
						status = js.toJSONString();
						authMicroService();
						noOfTry += 1;
					} else {
						JSONObject js = new JSONObject();
						js.put("Status", responseBody);
						status = js.toString();
						break;
					}
				}
			} catch (IOException e) {
				JSONObject js = new JSONObject();
				js.put("Status_error", "Error while uploading to Cloud");
				status = js.toString();
				e.printStackTrace();
			}
			logger.info(status);
		} while (noOfTry < 3);

		return status;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/getJobStatus" }, method = { RequestMethod.POST })
	public @ResponseBody String getJobStatus(ModelMap model, @RequestParam("Job_Id") String jobid,
			Principal principal) {

		// Get the current status of a jobID

		int noOfTry = 0;
		String resp = null;
		do {
			noOfTry += 1;
			CloseableHttpClient client = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(microservice + "/jobstatus");
			try {
				if (getMicroserviceToken() == null)
					authMicroService();
				httpPost.setHeader("Authorization", "Bearer " + getMicroserviceToken());
				ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
				postParameters.add(new BasicNameValuePair("jobId", jobid));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
				CloseableHttpResponse response = client.execute(httpPost);
				if (response.getStatusLine().getStatusCode() == 200) {
					BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					String readLine;
					String responseBody = "";
					while (((readLine = br.readLine()) != null)) {
						responseBody += readLine;
					}
					br.close();
					if ((responseBody.toString()).equalsIgnoreCase("Invalid Token")) {
						JSONObject js = new JSONObject();
						js.put("Auth_error", "Invalid Token");
						resp = js.toJSONString();
						authMicroService();
					} else {
						resp = responseBody;
						break;
					}
				}
			} catch (IOException e) {
				JSONObject js = new JSONObject();
				js.put("Status_error", "Error while fetching Job: " + jobid);
				resp = js.toJSONString();
			}
		} while (noOfTry < 3);
		return resp;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/searchkey" }, method = { RequestMethod.POST })
	public @ResponseBody JSONObject searchKey(@RequestParam("searchkey") String searchkey) {

		// Search all the devices and gateways for a specific keyword
		JSONObject resp = new JSONObject();
		List<GatewayModel> gateway = gatewayService.searchGateway(searchkey);
		List<DeviceModel> device = deviceService.searchDevices(searchkey);
		try {
			resp.put("gateways", new JSONParser().parse(new Gson().toJson(gateway)));
			resp.put("devices", new JSONParser().parse(new Gson().toJson(device)));
		} catch (ParseException e) {
			resp.put("error", "parsing");
		}
		return resp;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/startWebsocket" }, method = { RequestMethod.POST })
	public @ResponseBody JSONObject startWebsocket() {

		// Start the websocket Connectivity
		JSONObject resp = new JSONObject();
		ws_local.setup(); // Local Websocket 
		logger.info("Local Java UI Websocket Connected");
		if(kafka_enabled.equalsIgnoreCase("Y"))
		{
			try {
				kafka_endpoint.setup(ws_local);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.info("Kafka Connected");
			resp.put("Status", "Kafka connectDone");
		}
		
		else
		{
			try {
				iothubread_client.startClient(ws_local);
				resp.put("Status", "IoTHubWebapp connect Done");
			} catch (EventHubException e) {
				// TODO Auto-generated catch block
				resp.put("Status", "IoTHubWebapp connect EventHubException");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				resp.put("Status", "IoTHubWebapp connect IOException");
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				resp.put("Status", "IoTHubWebapp connect InterruptedException");
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				resp.put("Status", "IoTHubWebapp connect ExecutionException");
				e.printStackTrace();
			}
			
			
			//ws_dotnet.setup();
			//logger.info("Dotnet Websocket Connected");
			//ws_nodejs.setup();
			//logger.info("Nodejs Websocket Connected");
			
			//Commented by Sreenath for decoupling microservices,websockets.Enables direct iothub read
		
			/*ws_ms.setup(ws_local); 
		logger.info("net and Node JS Microservices Websocket Connected");
		int noOfTry = 0;
		
		do {
			noOfTry += 1;
			CloseableHttpClient client = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(microservice + "/startWebsocket");
			try {
				if (getMicroserviceToken() == null)
					authMicroService();
				httpPost.setHeader("Authorization", "Bearer " + getMicroserviceToken());
				ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
				postParameters.add(new BasicNameValuePair("dummy", "dummy"));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
				CloseableHttpResponse response = client.execute(httpPost);
				if (response.getStatusLine().getStatusCode() == 200) {
					BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					String readLine;
					String responseBody = "";
					while (((readLine = br.readLine()) != null)) {
						responseBody += readLine;
					}
					br.close();
					if ((responseBody.toString()).equalsIgnoreCase("Invalid Token")) {
						resp.put("Auth_error", "Invalid Token");
						authMicroService();
					} else {
						resp.put("Status", "Done");
						break;
					}
				}
			} catch (IOException e) {
				resp.put("Status_error", "Error while starting microserivces Websocket http auth");
			}
		} while (noOfTry < 3);*/ //End comment -Sreenath
		}
		return resp;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/save_calibration" }, method = { RequestMethod.POST })
	public @ResponseBody String save_Calibration(ModelMap model, Principal principal,
			@RequestParam("Device_Id") String Device_Id, @RequestParam("property") String property,
			@RequestParam("value") String value) throws ParseException {

		// Save Calibrated data to Database

		String flag = null;
		Calibration_Config cc = calibration_ConfigService.getByDeviceId(Device_Id);
		JSONObject calib_Olddata;
		if (cc.getDevice_Id() != null) {
			String oldproperty = cc.getProperty();
			if (oldproperty.isEmpty())
				calib_Olddata = new JSONObject();
			else
				calib_Olddata = (JSONObject) new JSONParser().parse(oldproperty);
			cc.setR_Mod_Time(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a").format(new Date()));
		} else {
			cc.setDevice_Id(Device_Id);
			cc.setR_Cre_Time(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a").format(new Date()));
			cc.setR_Mod_Time(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a").format(new Date()));
			calib_Olddata = new JSONObject();
		}
		calib_Olddata.put(property, value);
		cc.setProperty(calib_Olddata.toJSONString());
		calibration_ConfigService.saveOrUpdate(cc);
		flag = "Done";

		return flag;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/get_calibration" }, method = { RequestMethod.POST }) // Gives Complete Calibration
	public @ResponseBody String get_Calibration(ModelMap model, Principal principal,
			@RequestParam("Device_Id") String Device_Id) {

		// Return all the calibrated data for a device from database to dashboard

		Calibration_Config cc = calibration_ConfigService.getByDeviceId(Device_Id);
		JSONObject temp = new JSONObject();
		temp.put("Device_Id", Device_Id);
		if (cc.getDevice_Id() == null)
			temp.put("property", (new JSONObject()).toJSONString());
		else
			try {
				temp.put("property", (JSONObject) new JSONParser().parse(cc.getProperty()));
			} catch (ParseException e) {
				temp.put("property", (new JSONObject()).toJSONString());
			}
		return temp.toJSONString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/get_calibrationvalue" }, method = { RequestMethod.POST }) // Give single configuration
	public @ResponseBody String get_Calibrationvalue(ModelMap model, Principal principal,
			@RequestParam("Device_Id") String Device_Id, @RequestParam("property") String property) {
		
		//Return the calibration Value of a property of a device
		
		Calibration_Config cc = calibration_ConfigService.getByDeviceId(Device_Id);
		JSONObject temp = new JSONObject();
		temp.put("Device_Id", Device_Id);
		if (cc.getDevice_Id() == null)
			{
				temp.put("property", (new JSONObject()).toJSONString());
				return "0";
			}
		else {
			try {
				temp.put("property", (JSONObject) new JSONParser().parse(cc.getProperty()));
			} catch (ParseException e) {
				temp.put("property", (new JSONObject()).toJSONString());
			}
		}
		JSONObject prop;
		if (temp.containsKey("property"))
			prop = (JSONObject) temp.get("property");
		else
			prop = new JSONObject();
		if (prop.containsKey(property)) {
			return prop.get(property).toString();
		} else
			return "0";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/getgatewaydata" }, method = { RequestMethod.POST })
	public @ResponseBody String getgwdata(@RequestParam(value = "gateway") String gateway) {
		
		//Get Gateway table data
		
		InetAddress myIP = null;
		try {
			myIP = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		String IPAddress = myIP.getHostAddress();
		String hostname = myIP.getHostName();
		GatewayModel gw = gatewayService.getGateway(gateway);
		JSONObject gwjson = new JSONObject();
		try {
			gwjson.put("gatewaydata", new JSONParser().parse(new Gson().toJson(gw)));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		gwjson.put("hostname", hostname);
		gwjson.put("ipaddress", IPAddress);
		return gwjson.toJSONString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/registerdevice" }, method = { RequestMethod.POST })
	public @ResponseBody String registerdevice(HttpServletRequest request, ModelMap model,
			@RequestParam("device") String devicestring, Principal principal) {
		
		//Register a new device to database
		logger.info(devicestring);
		String status = "";
		int noOfTry = 0;
		do {
			noOfTry += 1;
			CloseableHttpClient client = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(microservice + "/registerdevice");
			try {
				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("device", devicestring));
				httpPost.setEntity(new UrlEncodedFormEntity(nvps));
				if (getMicroserviceToken() == null)
					authMicroService();
				httpPost.setHeader("Authorization", "Bearer " + getMicroserviceToken());
				CloseableHttpResponse response = client.execute(httpPost);
				if (response.getStatusLine().getStatusCode() == 200) {
					BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					String readLine;
					String responseBody = "";
					while (((readLine = br.readLine()) != null)) {
						responseBody += readLine;
					}
					br.close();
					if ((response.toString()).equalsIgnoreCase("Invalid Token")) {
						JSONObject js = new JSONObject();
						js.put("Status", "Auth Error: Invalid Token");
						status = js.toJSONString();
						authMicroService();
					} else {
						JSONObject js = new JSONObject();
						js.put("Status", responseBody);
						status = js.toString();
						break;
					}
				}
			} catch (IOException e) {
				JSONObject js = new JSONObject();
				js.put("Status_error", "Error while uploading to Cloud");
				status = js.toString();
				e.printStackTrace();
			}
			logger.info(status);
		} while (noOfTry < 3);
		return status;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/registergateway" }, method = { RequestMethod.POST })
	public @ResponseBody String registergateway(HttpServletRequest request, ModelMap model,
			@RequestParam("gateway") String gatewaystring, Principal principal) {
		
		//Register a new gateway in database
		
		String status = "";
		int noOfTry = 0;
		do {
			noOfTry += 1;
			CloseableHttpClient client = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(microservice + "/registergateway");
			try {
				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("gateway", gatewaystring));
				httpPost.setEntity(new UrlEncodedFormEntity(nvps));
				if (getMicroserviceToken() == null)
					authMicroService();
				httpPost.setHeader("Authorization", "Bearer " + getMicroserviceToken());
				CloseableHttpResponse response = client.execute(httpPost);
				if (response.getStatusLine().getStatusCode() == 200) {
					BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					String readLine;
					String responseBody = "";
					while (((readLine = br.readLine()) != null)) {
						responseBody += readLine;
					}
					br.close();
					if ((response.toString()).equalsIgnoreCase("Invalid Token")) {
						JSONObject js = new JSONObject();
						js.put("Status", "Auth Error: Invalid Token");
						status = js.toJSONString();
						authMicroService();
					} else {
						JSONObject js = new JSONObject();
						js.put("Status", responseBody);
						status = js.toString();
						break;
					}
				}
			} catch (IOException e) {
				JSONObject js = new JSONObject();
				js.put("Status_error", "Error while uploading to Cloud");
				status = js.toString();
				e.printStackTrace();
			}
			logger.info(status);
		} while (noOfTry < 3);
		return status;
	}

	public String getMicroserviceToken() {
		return microserviceToken;
	}

	public void setMicroserviceToken(String microserviceToken) {
		this.microserviceToken = microserviceToken;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/getNodejsDashboard" }, method = { RequestMethod.POST })
	public String getNodejsDashboard() {
		return "templates/nodejsPowerBIDatashboard";
	} 
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getdevicesFromAzure", method = RequestMethod.POST)
	public @ResponseBody String getDevicesFromAzure(ModelMap model) throws JsonSyntaxException, IOException, IotHubException {
		
		JSONObject jsonObject = new JSONObject();
		
		// Return all connected devices to a gateway
		//List<DeviceModel> currentGatewayDevices = deviceService.getDeviceByGatewayId(gateway.trim());
		List<DeviceModel> allGatewayDevices = deviceService.findall();
		
		ArrayList<Device> azureDeviceList = deviceService.getDevicesFromAzure();
		ArrayList<com.ey.iot.iotsphereeg.beans.Device> requiredDeviceList = new ArrayList<>();
		com.ey.iot.iotsphereeg.beans.Device oitDevice = null;
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		for(DeviceModel devices: allGatewayDevices){
			for(Device azurDevice: azureDeviceList){
				if(devices.getDevice_Id().equals(azurDevice.getDeviceId())){
					oitDevice = new com.ey.iot.iotsphereeg.beans.Device();
					oitDevice.setConnectionStateUpdatedTime(azurDevice.getConnectionStateUpdatedTime());
					oitDevice.setDeviceId(azurDevice.getDeviceId());
					oitDevice.setDeviceStatus(azurDevice.getConnectionState().toString());
					oitDevice.setGatewayId(devices.getGateway_Id());
					try {
						oitDevice.setLastActivityTime(sdf.parse(azurDevice.getLastActivityTime()));
					} catch (java.text.ParseException e) {
						e.printStackTrace();
					}
					oitDevice.setStatusUpdatedTime(azurDevice.getStatusUpdatedTime());
					requiredDeviceList.add(oitDevice);;
				}
			}
		}
		Collections.sort(requiredDeviceList);
		
		try {
			jsonObject.put("devices", new JSONParser().parse(new Gson().toJson(requiredDeviceList)));
			System.out.println(jsonObject.toJSONString());
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
		return jsonObject.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/iotassetoutputtracking" }, method = { RequestMethod.POST })
	public @ResponseBody String iotassetoutputtracking(HttpServletRequest request, ModelMap model,
			@RequestParam("assetinfo") String assetinfo, Principal principal) throws Exception {
		//input= // {"assetid":"Hyundai01", "make": "Hyundai","model":"2018","chassisno": "X32434","orderid": "O101","destination_country": "India", "origin_country": "US","shipmentid":"S01","driverid":"D101","mode":"ship"}
	//	{ "assetid":"Hyundai01", "orderid": "O101","shipmentid":"S01","driverid":"D101","mode":"ship","health":"good","delay_arrival":"5","physical_damage":"nil","savings":"2000","geofence_alert_cnt":"3","present_loc_lat":"10.234", "present_loc_long":"72.234"}

		logger.info(assetinfo);
		JSONParser parser = new JSONParser(); 
		try {
			JSONObject json = (JSONObject) parser.parse(assetinfo);
			if(json.containsKey("assetid")&&json.containsKey("orderid")&&json.containsKey("shipmentid")&&json.containsKey("driverid")&&json.containsKey("deviceid")&&json.containsKey("delay_arrival")&&json.containsKey("geofence_alert_cnt")&&json.containsKey("mode")&&json.containsKey("physical_damage")&&json.containsKey("present_loc_lat")&&json.containsKey("present_loc_long")&&json.containsKey("savings"))
			{
				AssetOutputModel assetoutputModel=new AssetOutputModel();
				
				assetoutputModel.setAssetid((String) json.get("assetid"));
				assetoutputModel.setOrderid((String) json.get("orderid"));
				assetoutputModel.setShipmentid((String) json.get("shipmentid"));
				assetoutputModel.setDriverid((String) json.get("driverid"));
				assetoutputModel.setDeviceid((String) json.get("deviceid"));
				assetoutputModel.setGeofence_alert_cnt((String) json.get("geofence_alert_cnt"));
				assetoutputModel.setDelay_arrival((String) json.get("delay_arrival"));
				assetoutputModel.setMode((String) json.get("mode"));
				assetoutputModel.setPhysical_damage((String) json.get("physical_damage"));
				assetoutputModel.setPresent_loc_lat((String) json.get("present_loc_lat"));
				assetoutputModel.setPresent_loc_long((String) json.get("present_loc_long"));
				assetoutputModel.setSavings((String) json.get("savings"));
				
				assetoutputModel.setDeleted(false);
				assetoutputModel.setRcretime((new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a").format(new Date())));
				assetoutputModel.setRModTime((new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a").format(new Date())));
				assetoutputservice.savedata(assetoutputModel);
			}
			else
			{
				return "Missing Key!";
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
			return "Error in processing";
		}

		
		return "Success Insert";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/cors/iotassetoutputfetch" }, method = { RequestMethod.POST })
	public @ResponseBody String iotassetoutputfetch(HttpServletRequest request, ModelMap model)
			 throws Exception {

		List<AssetOutputModel> assetinfo = assetoutputservice.finddata("");
		if(!assetinfo.isEmpty())
		{
			try
			{
		String json =new Gson().toJson(assetinfo);
		return json;
			}
			catch (Exception e)
			{
				return "Error";	
		}
		}
			return "0";

}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/iotassettracking" }, method = { RequestMethod.POST })
	public @ResponseBody String iotassettracking(HttpServletRequest request, ModelMap model,
			@RequestParam("assetinfo") String assetinfo, Principal principal) throws Exception {
		//input= // {"assetid":"Hyundai01", "make": "Hyundai","model":"2018","chassisno": "X32434","orderid": "O101","destination_country": "India", "origin_country": "US","shipmentid":"S01","driverid":"D101","mode":"ship"}
	//	{ "assetid":"Hyundai01", "orderid": "O101","shipmentid":"S01","driverid":"D101","mode":"ship","health":"good","delay_arrival":"5","physical_damage":"nil","savings":"2000","geofence_alert_cnt":"3","present_loc_lat":"10.234", "present_loc_long":"72.234"}

		logger.info(assetinfo);
		JSONParser parser = new JSONParser(); 
		try {
			JSONObject json = (JSONObject) parser.parse(assetinfo);
			if(json.containsKey("assetid")&&json.containsKey("orderid")&&json.containsKey("shipmentid")&&json.containsKey("driverid")&&json.containsKey("make")&&json.containsKey("model")&&json.containsKey("chassisno")&&json.containsKey("destination_country")&&json.containsKey("origin_country"))
			{
				AssetInputModel assetinputModel=new AssetInputModel();
				
				assetinputModel.setAssetid((String) json.get("assetid"));
				assetinputModel.setOrderid((String) json.get("orderid"));
				assetinputModel.setShipmentid((String) json.get("shipmentid"));
				assetinputModel.setDriverid((String) json.get("driverid"));
				assetinputModel.setMake((String) json.get("make"));
				assetinputModel.setModel((String) json.get("model"));
				assetinputModel.setChassisno((String) json.get("chassisno"));
				assetinputModel.setDestination_country((String) json.get("destination_country"));
				assetinputModel.setOrigin_country((String) json.get("origin_country"));
				assetinputModel.setDeleted(false);
				assetinputModel.setRcretime((new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a").format(new Date())));
				assetinputModel.setRModTime((new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a").format(new Date())));
				assetinputservice.savedata(assetinputModel);
			}
			else
			{
				return "Missing Key!";
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
			return "Error in processing";
		}

		
		return "Success Insert";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/cors/iotassetinputfetch" }, method = { RequestMethod.POST })
	public @ResponseBody String iotassetinputfetch(HttpServletRequest request, ModelMap model)
			 throws Exception {

		List<AssetInputModel> assetinfo = assetinputservice.finddata("");
		if(!assetinfo.isEmpty())
		{
			try
			{
		String json =new Gson().toJson(assetinfo);
		return json;
			}
			catch (Exception e)
			{
				return "Error";	
		}
		}
			return "0";

}


}
