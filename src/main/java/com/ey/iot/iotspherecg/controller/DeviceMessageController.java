package com.ey.iot.iotspherecg.controller;

import java.util.Iterator;
import java.util.Set;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import com.ey.iot.iotspherecg.model.Calibration_Config;
import com.ey.iot.iotspherecg.model.DeviceMessage;
import com.ey.iot.iotspherecg.service.Calibration_ConfigService;

@Controller
@DependsOn("calibration_ConfigServiceImpl")
public class DeviceMessageController {
	// private static Logger logger =
	// Logger.getLogger(DeviceMessageController.class);
	@Autowired
	private Calibration_ConfigService calibration_ConfigService;

	@SuppressWarnings("unchecked")
	@MessageMapping("/chat.sendMessage")
	@SendTo("/channel/public")
	public DeviceMessage sendMessage(@Payload DeviceMessage deviceMessage) {
		JSONObject tempJSON = deviceMessage.getContent();
		String deviceId = null;
		if (tempJSON.containsKey("DeviceId"))
			deviceId = tempJSON.get("DeviceId").toString();
		if (deviceId != null) {
			Calibration_Config cc = calibration_ConfigService.getByDeviceId(deviceId);
			JSONObject oldcal;
			try {
				if (cc.getProperty() == null)
					oldcal = new JSONObject();
				else
					oldcal = (JSONObject) new JSONParser().parse(cc.getProperty());
			} catch (ParseException e) {
				oldcal = new JSONObject();
			}
			Set<?> i = oldcal.keySet();
			Iterator<?> keys = i.iterator();
			JSONObject newdata;
			newdata = tempJSON;
			while (keys.hasNext()) {
				String key = (String) keys.next();
				if (newdata.containsKey(key)) {
					float temp_old = Float.parseFloat(oldcal.get(key).toString());
					float temp_new = Float.parseFloat(newdata.get(key).toString());
					float temp_update = temp_old + temp_new;
					newdata.put(key, "" + temp_update);
				}
			}
			deviceMessage.setContent(newdata);
		}
		return deviceMessage;
	}

	@MessageMapping("/chat.addDevice")
	// @SendTo("/channel/public")
	public void addUser(@Payload DeviceMessage deviceMessage, SimpMessageHeaderAccessor headerAccessor) {
		// logger.info("New Client "+deviceMessage.getSender()+" Connected.");
		headerAccessor.getSessionAttributes().put("deviceId", deviceMessage.getSender());
	}

}
