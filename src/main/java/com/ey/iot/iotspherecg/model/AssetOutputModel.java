package com.ey.iot.iotspherecg.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@DynamicUpdate(true)
@SelectBeforeUpdate
@Table(name = "ASSETOUTPUTINFO")
public class AssetOutputModel implements Serializable {

	/**
	 * AssetModel
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", unique = true, nullable = false)	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@Column(name = "R_Cre_Time", nullable = false)
	private String rcretime;
	@Column(name = "Deviceid", nullable = true)
	private String Deviceid;
	@NotEmpty
	@Column(name = "assetid", unique = true,nullable = false)
	private String assetid;
	@NotEmpty
	@Column(name = "orderid", nullable = false)
	private String orderid;
	@Column(name = "shipmentid", nullable = false)
	private String shipmentid;
	@Column(name = "driverid", nullable = true)
	private String driverid;
	@Column(name = "mode", nullable = true)
	private String mode;
	@Column(name = "health", nullable = true)
	private String health;
	@Column(name = "delay_arrival", nullable = true)
	private String delay_arrival;
	@Column(name = "physical_damage", nullable = true)
	private String physical_damage;
	@Column(name = "savings", nullable = true)
	private String savings;
	@Column(name = "geofence_alert_cnt", nullable = true)
	private String geofence_alert_cnt;
	@Column(name = "present_loc_lat", nullable = true)
	private String present_loc_lat;
	@Column(name = "present_loc_long", nullable = true)
	private String present_loc_long;
	@Column(name = "Freefld1", nullable = true)
	private String Freefld1;
	@Column(name = "Freefld2", nullable = true)
	private String Freefld2;
	@Column(name = "R_Mod_Time", nullable = true)
	private String RModTime;
	@Column(name = "deleted", nullable = false)
	private boolean deleted;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getRcretime() {
		return rcretime;
	}
	public void setRcretime(String rcretime) {
		this.rcretime = rcretime;
	}
	public String getDeviceid() {
		return Deviceid;
	}
	public void setDeviceid(String deviceid) {
		Deviceid = deviceid;
	}
	public String getAssetid() {
		return assetid;
	}
	public void setAssetid(String assetid) {
		this.assetid = assetid;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getShipmentid() {
		return shipmentid;
	}
	public void setShipmentid(String shipmentid) {
		this.shipmentid = shipmentid;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getHealth() {
		return health;
	}
	public void setHealth(String health) {
		this.health = health;
	}
	public String getDelay_arrival() {
		return delay_arrival;
	}
	public void setDelay_arrival(String delay_arrival) {
		this.delay_arrival = delay_arrival;
	}
	public String getPhysical_damage() {
		return physical_damage;
	}
	public void setPhysical_damage(String physical_damage) {
		this.physical_damage = physical_damage;
	}
	public String getSavings() {
		return savings;
	}
	public void setSavings(String savings) {
		this.savings = savings;
	}
	public String getGeofence_alert_cnt() {
		return geofence_alert_cnt;
	}
	public void setGeofence_alert_cnt(String geofence_alert_cnt) {
		this.geofence_alert_cnt = geofence_alert_cnt;
	}
	public String getPresent_loc_lat() {
		return present_loc_lat;
	}
	public void setPresent_loc_lat(String present_loc_lat) {
		this.present_loc_lat = present_loc_lat;
	}
	public String getPresent_loc_long() {
		return present_loc_long;
	}
	public void setPresent_loc_long(String present_loc_long) {
		this.present_loc_long = present_loc_long;
	}
	public String getFreefld1() {
		return Freefld1;
	}
	public void setFreefld1(String freefld1) {
		Freefld1 = freefld1;
	}
	public String getFreefld2() {
		return Freefld2;
	}
	public void setFreefld2(String freefld2) {
		Freefld2 = freefld2;
	}
	public String getRModTime() {
		return RModTime;
	}
	public void setRModTime(String rModTime) {
		RModTime = rModTime;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	
}
