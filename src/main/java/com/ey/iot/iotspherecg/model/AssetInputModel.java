package com.ey.iot.iotspherecg.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@DynamicUpdate(true)
@SelectBeforeUpdate
@Table(name = "ASSETINPUTINFO")
public class AssetInputModel implements Serializable {

	/**
	 * AssetModel
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", unique = true, nullable = false)	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@Column(name = "r_cre_time", nullable = false)
	private String rcretime;
	@Column(name = "deviceid", nullable = true)
	private String Deviceid;
	@NotEmpty
	@Column(name = "assetid", nullable = false, unique = true)
	private String assetid;
	@Column(name = "orderid", nullable = false)
	private String orderid;
	@Column(name = "shipmentid", nullable = false)
	private String shipmentid;
	@Column(name = "driverid", nullable = true)
	private String driverid;
	@Column(name = "make", nullable = true)
	private String make;
	@Column(name = "model", nullable = true)
	private String model;
	@Column(name = "model_car", nullable = true)
	private String model_car;
	@Column(name = "chassisno", nullable = false,unique = true)
	private String chassisno;
	@Column(name = "destination_country", nullable = true)
	private String destination_country;
	@Column(name = "origin_country", nullable = true)
	private String origin_country;
	@Column(name = "Freefld1", nullable = true)
	private String Freefld1;
	@Column(name = "Freefld2", nullable = true)
	private String Freefld2;
	@Column(name = "r_mod_time", nullable = false)
	private String RModTime;
	@Column(name = "deleted", nullable = false)
	private boolean deleted;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getRcretime() {
		return rcretime;
	}
	public void setRcretime(String rcretime) {
		this.rcretime = rcretime;
	}
	public String getDeviceid() {
		return Deviceid;
	}
	public void setDeviceid(String deviceid) {
		Deviceid = deviceid;
	}
	public String getAssetid() {
		return assetid;
	}
	public void setAssetid(String assetid) {
		this.assetid = assetid;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getShipmentid() {
		return shipmentid;
	}
	public void setShipmentid(String shipmentid) {
		this.shipmentid = shipmentid;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getModel_car() {
		return model_car;
	}
	public void setModel_car(String model_car) {
		this.model_car = model_car;
	}
	public String getChassisno() {
		return chassisno;
	}
	public void setChassisno(String chassisno) {
		this.chassisno = chassisno;
	}
	public String getDestination_country() {
		return destination_country;
	}
	public void setDestination_country(String destination_country) {
		this.destination_country = destination_country;
	}
	public String getOrigin_country() {
		return origin_country;
	}
	public void setOrigin_country(String origin_country) {
		this.origin_country = origin_country;
	}
	public String getFreefld1() {
		return Freefld1;
	}
	public void setFreefld1(String freefld1) {
		Freefld1 = freefld1;
	}
	public String getFreefld2() {
		return Freefld2;
	}
	public void setFreefld2(String freefld2) {
		Freefld2 = freefld2;
	}
	public String getRModTime() {
		return RModTime;
	}
	public void setRModTime(String rModTime) {
		RModTime = rModTime;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
